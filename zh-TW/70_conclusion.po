# AUTHOR <EMAIL@ADDRESS>, YEAR.
msgid ""
msgstr ""
"Project-Id-Version: 0\n"
"POT-Creation-Date: 2020-05-17 17:54+0200\n"
"PO-Revision-Date: 2019-01-15 08:56+0000\n"
"Last-Translator: Louies <louies0623@gmail.com>\n"
"Language-Team: Chinese (Traditional) <https://hosted.weblate.org/projects/debian-handbook/70_conclusion/zh_Hant/>\n"
"Language: zh-TW\n"
"MIME-Version: 1.0\n"
"Content-Type: application/x-publican; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Weblate 3.4-dev\n"

msgid "Future"
msgstr "未來"

msgid "Improvements"
msgstr "改進"

msgid "Opinions"
msgstr "意見"

msgid "Conclusion: Debian's Future"
msgstr "結論：Debian的未來"

msgid "The story of Falcot Corp ends with this last chapter; but Debian lives on, and the future will certainly bring many interesting surprises."
msgstr "Falcot Corp 公司的故事以這最後一章結束；但 Debian 仍繼續存在，未來肯定會帶來許多有趣的驚喜給您。"

msgid "Upcoming Developments"
msgstr "未來發展"

#, fuzzy
#| msgid "Weeks (or months) before a new version of Debian is released, the Release Manager picks the codename for the next version. Now that Debian version 8 is out, the developers are already busy working on the next version, codenamed <emphasis role=\"distribution\">Stretch</emphasis>…"
msgid "Now that Debian version 10 is out, the developers are already busy working on the next version, codenamed <emphasis role=\"distribution\">Bullseye</emphasis>…"
msgstr "在發佈 Debian 的新版本之前的幾周（或幾個月），發佈管理器會為下一個版本選擇代號。現在 Debian 第 8 版本已經推出，開發人員已經在忙著開發下一個版本，代號為<emphasis role=\"distribution\">伸展。</emphasis>…"

#, fuzzy
#| msgid "There is no official list of planned changes, and Debian never makes promises relating to technical goals of the coming versions. However, a few development trends can already be noted, and we can try some bets on what might happen (or not)."
msgid "There is no official list of planned changes, and Debian never makes promises relating to technical goals of the coming versions. However, a few development trends can already be noted, and we can try to guess what might happen (or not)."
msgstr "沒有正式的計畫變更清單，Debian 從未對即將到來的版本中，科技目標做出的承諾。但已經注意到一些發展趨勢，我們可以嘗試對可能發生的事情（或不發生的事情）來下賭注。"

#, fuzzy
#| msgid "In order to improve security and trust, most if not all the packages will be made to build reproducibly; that is to say, it will be possible to rebuild byte-for-byte identical binary packages from the source packages, thus allowing everyone to verify that no tampering has happened during the builds."
msgid "In order to improve security and trust, an increasing number of packages will be made to build reproducibly; that is to say, it will be possible to rebuild byte-for-byte identical binary packages from the source packages, thus allowing everyone to verify that no tampering has happened during the builds. This feature might even be required by the release managers for testing migration."
msgstr "為了提高安全性和信任度，大多數（如果不是所有的）套件都將被製作成可複製的建構；也就是說，可以從源包中重建逐位元組相同的二進位套件，從而允許每個人驗證在建構期間沒有發生篡改。"

#, fuzzy
#| msgid "In a related theme, a lot of effort will have gone into improving security by default, and mitigating both “traditional” attacks and the new threats implied by mass surveillance."
msgid "In a related theme, a lot of effort will have gone into improving security by default, with more packages shipping an AppArmor profile."
msgstr "在一個相關的主題中，許多工作將致力於在預設情况下改善安全，減輕“傳統”攻擊和大規模監視所暗示的新威脅。"

#, fuzzy
#| msgid "Of course, all the main software suites will have had a major release. The latest version of the various desktops will bring better usability and new features. Wayland, the new display server that is being developed to replace X11 with a more modern alternative, will be available (although maybe not default) for at least some desktop environments."
msgid "Of course, all the main software suites will have had a major release. The latest version of the various desktops will bring better usability and new features. Wayland, the new display server, will likely obsolete X11 entirely."
msgstr "當然，所有主要的軟體套件都將有一個主要的版本。各種桌上型電腦的最新版本將帶來更好的可用性和新功能。Wayland 是一款新的顯示服務器，現在還正在開發中，它將以更現代的方法來取代X 11，至少在某些專案環境中可以使用（儘管可能不是預設的）。"

msgid "With the widespread use of continuous integration and the growth of the archive (and of the biggest packages!), the constraints on release architectures will be harder to meet and some architectures will be dropped (like <emphasis>mips</emphasis>, <emphasis>mipsel</emphasis> and maybe <emphasis>mips64el</emphasis>)."
msgstr ""

msgid "Debian's Future"
msgstr "Debian 的未來"

#, fuzzy
#| msgid "In addition to these internal developments, one can reasonably expect new Debian-based distributions to come to light, as many tools keep making this task easier. New specialized subprojects will also be started, in order to widen Debian's reach to new horizons."
msgid "In addition to these internal developments, one can reasonably expect new Debian-based distributions to come to light, as many tools keep simplifying this task. New specialized subprojects will also be started, in order to widen Debian's reach to new horizons."
msgstr "除了這些內部開發之外，我們可以合理地期望新的基於 Debian 的發行版本出現，因為許多工具使這項任務變得更容易。新的專業化子項目也將啟動，以擴大 Debian 的新視野。"

msgid "The Debian user community will increase, and new contributors will join the project… including, maybe, you!"
msgstr "Debian 用戶社區將繼續新增，新的貢獻者將加入該項目…可能也包括你！"

msgid "There are recurring discussions about how the software ecosystem is evolving, towards applications shipped within containers, where Debian packages have no added value, or with language-specific package managers (e.g. <command>pip</command> for Python, <command>npm</command> for JavaScript, etc.), which are rendering <command>dpkg</command> and <command>apt</command> obsolete. Facing those threats, I am convinced that Debian developers will find ways to embrace those evolutions and to continue to provide value to users."
msgstr ""

msgid "In spite of its old age and its respectable size, Debian keeps on growing in all kinds of (sometimes unexpected) directions. Contributors are teeming with ideas, and discussions on development mailing lists, even when they look like bickerings, keep increasing the momentum. Debian is sometimes compared to a black hole, of such density that any new free software project is attracted."
msgstr "儘管 Debian 已經很老了，體型也相當可觀，但它仍然在向各種（有時是意想不到的）方向發展。投稿人充滿了想法，開發郵寄清單上的討論，即使它們看起來像爭吵，也在不斷增加勢頭。Debian 有時被比作黑洞，其密度足以吸引任何新的自由軟體項目。"

#, fuzzy
#| msgid "Beyond the apparent satisfaction of most Debian users, a deep trend is becoming more and more indisputable: people are increasingly realising that collaborating, rather than working alone in their corner, leads to better results for everyone. Such is the rationale used by distributions merging into Debian by way of subprojects."
msgid "Beyond the apparent satisfaction of most Debian users, a deep trend is becoming more and more indisputable: people are increasingly realizing that collaborating, rather than working alone in their corner, leads to better results for everyone. Such is the rationale used by distributions merging into Debian by way of subprojects."
msgstr "除了大多數 Debian 用戶明顯的滿意之外，一個深層次的趨勢變得越來越令人無可爭辯：人們越來越意識到，合作，而不是單獨在他們的角落工作，會為每個人帶來更好的結果。這就是通過子項目合併到 Debian 中的分佈所使用的基本原理。"

msgid "The Debian project is therefore not threatened by extinction…"
msgstr "因此，Debian 項目不會受到滅絕的威脅…"

msgid "Future of this Book"
msgstr "這本書的未來"

#, fuzzy
#| msgid "We would like this book to evolve in the spirit of free software. We therefore welcome contributions, remarks, suggestions, and criticism. Please direct them to Raphaël (<email>hertzog@debian.org</email>) or Roland (<email>lolando@debian.org</email>). For actionable feedback, feel free to open bug reports against the <literal>debian-handbook</literal> Debian package. The website will be used to gather all information relevant to its evolution, and you will find there information on how to contribute, in particular if you want to translate this book to make it available to an even larger public than today. <ulink type=\"block\" url=\"http://debian-handbook.info/\" />"
msgid "We would like this book to evolve in the spirit of free software. We therefore welcome contributions, remarks, suggestions, and criticism. Please direct them to Raphaël (<email>hertzog@debian.org</email>) or Roland (<email>lolando@debian.org</email>). For actionable feedback, feel free to open bug reports against the <literal>debian-handbook</literal> Debian package. The website will be used to gather all information relevant to its evolution, and you will find there information on how to contribute, in particular if you want to translate this book to make it available to an even larger public than today. <ulink type=\"block\" url=\"https://debian-handbook.info/\" />"
msgstr "我們希望這本書本著自由軟體的精神發展。因此, 我們歡迎各種意見、評論、建議和批評。請將評論內容寄給 Raphaël (<email>hertzog@debian.org</email>)或是 Roland (<email>lolando@debian.org</email>)。有關可操作的回饋，請隨時打開 <literal> 針對 Debian 手冊 Debian </literal> 套件軟體的錯誤報告。該網站將被用來收集與其演變相關的所有資訊，你會發現有如何貢獻的資訊，特別是如果你想翻譯這本書，使它提供給比以前更廣泛的大眾來閱讀。<ulink type=\"block\" url=\"http://debian-handbook.info/\" />"

#, fuzzy
#| msgid "We tried to integrate most of what our experience at Debian taught us, so that anyone can use this distribution and take the best advantage of it as soon as possible. We hope this book contributes to making Debian less confusing and more popular, and we welcome publicity around it!"
msgid "We tried to integrate most of what our experience with Debian taught us, so that anyone can use this distribution and take the best advantage of it as soon as possible. We hope this book contributes to making Debian less confusing and more popular, and we welcome publicity around it!"
msgstr "我們試圖在整合我們在 Debian 所教我們的大部分經驗，這樣任何人都可以使用這個分佈，並儘快利用它。我們希望這本書有助於减少 Debian 的混亂和更受歡迎，我們歡迎有關它的文宣！"

#, fuzzy
#| msgid "We would like to conclude on a personal note. Writing (and translating) this book took a considerable amount of time out of our usual professional activity. Since we are both freelance consultants, any new source of income grants us the freedom to spend more time improving Debian; we hope this book to be successful and to contribute to this. In the meantime, feel free to retain our services! <ulink type=\"block\" url=\"http://www.freexian.com\" /> <ulink type=\"block\" url=\"http://www.gnurandal.com\" />"
msgid "We would like to conclude on a personal note. Writing (and translating) this book took a considerable amount of time out of our usual professional activity. Since we are both freelance consultants, any new source of income grants us the freedom to spend more time improving Debian; we hope this book to be successful and to contribute to this. In the meantime, feel free to retain our services! <ulink type=\"block\" url=\"https://www.freexian.com\" /> <ulink type=\"block\" url=\"http://www.gnurandal.com\" />"
msgstr "我們想在一份私人檔案上作個總結。寫（譯）這本書花了我們日常職業活動相當長的時間。由於我們都是自由職業顧問，任何新的收入來源都賦予我們更多時間改進 Debian 的自由；我們希望這本書能夠成功，並為此作出貢獻。同時，請隨時關注我們的服務！<ulink type=\"block\" url=\"http://www.freexian.com\" /><ulink type=\"block\" url=\"http://www.gnurandal.com\" />"

msgid "See you soon!"
msgstr "期待能與您見面！"

#~ msgid "A new feature of the archive maintenance software, “bikesheds”, will allow developers to host special-purpose package repositories in addition to the main repositories; this will allow for personal package repositories, repositories for software not ready to go into the main archive, repositories for software that has only a very small audience, temporary repositories for testing new ideas, and so on."
#~ msgstr "檔案維護軟體的一個新功能 \"bikesheds\" 將使開發人員能夠在主存儲庫之外，託管專用的套件軟體存儲庫;這將允許個人套件軟體存儲庫，尚未準備好進入主存檔的套件存儲庫，只有非常小的受眾的套件存儲庫，用於測試新想法的臨時存儲庫等。"

#~ msgid "The Debian project is stronger than ever, and well on its way towards its goal of a universal distribution; the inside joke within the Debian community is about <foreignphrase>World Domination</foreignphrase>."
#~ msgstr "Debian 項目比以往任何時候都要强大，並且正朝著其普及的目標前進；而 Debian 社區內部的笑話是關於<foreignphrase>世界爭議</foreignphrase>。"
